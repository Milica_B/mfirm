

## MFirm

MFirm is an employee and office equipment management web application written in Laravel (PHP/MySQL).
Current version covers most tasks required by an office manager but more functionality will be added in the future, e.g., filling and printing official documents.
 

## Installing
<ul>
    <li>Create .env file</li>
    <li>CD into the directory of this project and run the
    following commands:
        <ul type="circle">
            <li>composer install</li>
            <li>php artisan key:generate</li>
            <li>  php artisan migrate</li>
            <li>  php artisan db:seed</li>
        </ul>
     </li>
</ul>
     
## Contributing

Contributions are encouraged and welcome.

