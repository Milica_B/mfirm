<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Employee extends Model
{
    protected $dates = ['birthday', 'start_date', 'end_date'];
    protected $fillable = ['name', 'last_name', 'email', 'municipality', 'phone', 'birthday', 'start_date', 'end_date'];

    public function salaries()
    {
        return $this->hasMany('App\Salary');
    }

    public function positions()
    {
        return $this->hasMany('App\Position');
    }

    public function debits()
    {
        return $this->hasMany('App\Debit');
    }

    public function fullName()
    {
        return $this->name . ' ' . $this->last_name;
    }

    public function freeDebit()
    {
        $debits = $this->debits;
        foreach ($debits as $debit) {
            if ($debit->end_date == null) {
                return false;
            }
        }
        return true;
    }

    public static function isBirthdayOrAnniwersary()
    {
        $employees = Employee::where('active', '1')->get();
        $today = Carbon::now();
        $output = "<ul>";
        foreach ($employees as $employee) {
            if ($today->diffInMonths($employee->birthday) % 12 == 0) {
                $birthday = $employee->birthday->addMonth($today->diffInMonths($employee->birthday));
                if ($today->diffInDays($birthday, false) == 0) {
                    $output .= "<li> Today is " . $employee->fullName() . " birthday </li>";
                }
                if ($today->diffInDays($birthday, false) == 1) {
                    $output .= '<li> In ' . $today->diffInDays($birthday, false) . ' day is ' . $employee->fullName() . "'s birthday.</li>";
                }
                if ($today->diffInDays($birthday, false) < 5 && $today->diffInDays($birthday, false) > 1) {
                    $output .= '<li> In ' . $today->diffInDays($birthday, false) . ' days is ' . $employee->fullName() . "'s birthday.</li>";
                }

            }
            if ($today->diffInMonths($employee->start_date) % 12 == 0) {
                $anniwersary = $employee->start_date->addMonth($today->diffInMonths($employee->start_date));
                if ($today->diffInDays($anniwersary, false) == 0) {
                    $output .= "<li> Today is " . $employee->fullName() . "'s work anniversary </li>";
                }
                if ($today->diffInDays($anniwersary, false) == 1) {
                    $output .= '<li> In ' . $today->diffInDays($anniwersary, false) . ' day is ' . $employee->fullName() . "'s work anniwersary.</li>";
                }
                if ($today->diffInDays($anniwersary, false) < 5 && $today->diffInDays($anniwersary, false) > 1) {
                    $output .= '<li> In ' . $today->diffInDays($anniwersary, false) . ' days is ' . $employee->fullName() . "'s work anniwersary.</li>";
                }
            }

        }
        $output .= '</ul>';
        return $output !== '<ul></ul>' ? $output : null;
    }
}
