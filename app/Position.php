<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Position extends Model
{
    protected $dates = ['date'];

    public function employee()
    {
        return $this->belongsTo('App\Employee');
    }
}
