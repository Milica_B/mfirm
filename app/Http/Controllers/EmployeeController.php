<?php

namespace App\Http\Controllers;

use App\Employee;
use Illuminate\Http\Request;
use App\Salary;
use App\Position;

class EmployeeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getEmployeesList($active)
    {
        if ($active !== '0' && $active !== '1') {
            return abort('404');
        }
        if ($active == 1) {
            $employees = Employee::where('active', 1)->paginate(10);
        }
        if ($active == 0) {
            $employees = Employee::where('active', 0)->paginate(10);
        }
        return view('employee.employees_list', ['employees' => $employees, 'active' => $active]);
    }

    public function getCreateEmployee()
    {
        return view('employee.create_employee');
    }

    public function postCreateEmployee(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:30',
            'last_name' => 'required|max:40',
            'email' => 'required|email|unique:employees',
            'phone' => 'required',
            'municipality' => 'required',
            'birthday' => 'required|date|after:01/01/1950',
            'start_date' => 'required|date|after:01/01/2015'
        ]);
        $employee = new Employee();
        $employee->name = ucfirst(strtolower($request['name']));
        $employee->last_name = ucfirst(strtolower($request['last_name']));
        $employee->email = $request['email'];
        $employee->phone = $request['phone'];
        $employee->municipality = $request['municipality'];
        $employee->birthday = $request['birthday'];
        $employee->start_date = $request['start_date'];
        $employee->active = 1;
        if ($employee->save()) {
            return redirect()->route('employees_list', ['active' => 1])->with(['success' => 'You successfully added new employee']);
        }
        return redirect()->route('employees_list', ['active' => 1])->with(['fail' => 'Something went wrong, new employee is not added']);
    }

    public function getEmployeeIndex($id)
    {
        if ($employee = Employee::find($id)) {
            return view('employee.employee', ['employee' => $employee]);
        }
        return abort('404');
    }

    public function getQuitJob($id)
    {
        if ($employee = Employee::find($id)) {
            if (!$employee->freeDebit()) {
                return redirect()->route('employee_index', ['id' => $employee->id])->with(['fail' => 'There are some equipments that are still in ' . $employee->fullName() . ' possession.']);
            }
            return view('employee.quit_job', ['employee' => $employee]);
        }
        return abort('404');
    }

    public function patchQuitJob(Request $request, $id)
    {
        $this->validate($request, [
            'end_date' => 'required|date'
        ]);
        if ($employee = Employee::find($id)) {

            $employee->end_date = $request['end_date'];
            $employee->active = 0;
            $salary = new Salary();
            $salary->amount = 0;
            $salary->comment = "Not working";
            $salary->date = $request['end_date'];
            $salary->valid = 1;
            $position = new Position();
            $position->name = "Not working";
            $position->type_of_employment = "Not working";
            $position->date = $request['end_date'];
            $position->comment = ucfirst($request['comment']);
            $position->valid = 1;

            if ($employee->update() && $employee->salaries()->save($salary) && $employee->positions()->save($position)) {
                return redirect()->route('employee_index', ['id' => $employee->id])->with(['success' => $employee->fullName() . ' quitted job']);
            }
            return redirect()->route('quit_job', ['id' => $employee->id])->with(['fail' => 'Something went wrong']);
        }
        return abort('404');
    }

    public function getChangeEntries($id)
    {
        if ($employee = Employee::find($id)) {
            return view('employee.change_entries', ['employee' => $employee]);
        }
        return abort('404');
    }

    public function patchChangeEntries(Request $request, $id)
    {
        if ($employee = Employee::find($id)) {
            $this->validate($request, [
                'name' => 'required|max:30',
                'last_name' => 'required|max:40',
                'email' => 'required|email',
                'phone' => 'required',
                'municipality' => 'required',
                'birthday' => 'required|date|after:01/01/1950',
                'start_date' => 'required|date|after:01/01/2015'
            ]);
            if ($employee->update($request->all())) {
                return redirect()->route('employee_index', ['id' => $id])->with(['success' => 'You successfully changed entries']);
            }
            return redirect()->route('employee_index', ['id' => $id])->with(['fail' => 'Something went wrong,entries are not changed']);
        }
        return abort('404');
    }

    public function getWorkAgain($id)
    {
        if ($employee = Employee::find($id)) {
            return view('employee.work_again', ['employee' => $employee]);
        }
        return abort('404');
    }

    public function patchWorkAgain(Request $request, $id)
    {
        if ($employee = Employee::find($id)) {
            $this->validate($request, [
                'start_date' => 'required|date',
                'amount' => 'required|min:0',
                'name' => 'required|max:100',
                'type_of_employment' => 'required|max:100'
            ]);
            $employee->start_date = $request['start_date'];
            $employee->end_date = null;
            $employee->active = 1;
            $salary = new Salary();
            $salary->amount = $request['amount'];
            $salary->comment = ucfirst($request['comment_salary']);
            $salary->date = $request['start_date'];
            $salary->valid = 1;
            $position = new Position();
            $position->name = ucfirst($request['name']);
            $position->type_of_employment = ucfirst($request['type_of_employment']);
            $position->date = $request['start_date'];
            $position->comment = ucfirst($request['comment_position']);
            $position->valid = 1;

            if ($employee->update() && $employee->salaries()->save($salary) && $employee->positions()->save($position)) {
                return redirect()->route('employee_index', ['id' => $id])->with(['success' => 'You successfully changed working status']);
            }
            return redirect()->route('employee_index', ['id' => $id])->with(['fail' => 'Something went wrong,working status is not changed']);
        }
        return abort('404');
    }
}