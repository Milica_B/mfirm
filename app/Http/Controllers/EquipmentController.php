<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Type;
use App\Equipment;


class EquipmentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getAddEquipment($type_id)
    {
        if ($type = Type::find($type_id)) {
            return view('equipment.add', ['type' => $type]);
        }
        return abort('404');
    }

    public function postAddEquipment(Request $request, $type_id)
    {
        if ($type = Type::find($type_id)) {

            $this->validate($request, [
                'brand' => 'required|max:80',
                'model' => 'required|max:80',
                'serial_number' => 'required|max:80',
                'inventory_number' => 'required|max:80',
                'purchase_date' => 'required|date',
                'guarantee' => 'required|min:0'
            ]);

            $equipment = new Equipment();
            $equipment->brand = ucfirst($request['brand']);
            $equipment->model = ucfirst($request['model']);
            $equipment->serial_number = $request['serial_number'];
            $equipment->inventory_number = $request['inventory_number'];
            $equipment->purchase_date = $request['purchase_date'];
            $equipment->guarantee = $request['guarantee'];
            $equipment->working = 0;
            $equipment->comment = ucfirst($request['comment']);

            if ($type->equipments()->save($equipment)) {
                return redirect()->route('equipments', ['working' => 0, 'type_id' => $type->id])->with(['success' => 'You successfully added new ' . lcfirst($type->name)]);
            }
            return redirect()->route('equipments', ['working' => 0, 'type_id' => $type->id])->with(['fail' => 'Something went wrong']);
        }
        return abort('404');
    }

    public function getEquipment($id)
    {
        if ($equipment = Equipment::find($id)) {
            return view('equipment.equipment', ['equipment' => $equipment]);
        }
        return abort('404');
    }

    public function getEditEquipment($id)
    {
        if ($equipment = Equipment::find($id)) {
            return view('equipment.change_entries', ['equipment' => $equipment]);
        }
        return abort('404');
    }

    public function patchEditEquipment(Request $request, $id)
    {
        if ($equipment = Equipment::find($id)) {
            $this->validate($request, [
                'brand' => 'required|max:80',
                'model' => 'required|max:80',
                'serial_number' => 'required|max:80',
                'inventory_number' => 'required|max:80',
                'purchase_date' => 'required|date',
                'guarantee' => 'required|min:0'
            ]);
            $equipment->brand = ucfirst($request['brand']);
            $equipment->model = ucfirst($request['model']);
            $equipment->serial_number = $request['serial_number'];
            $equipment->inventory_number = $request['inventory_number'];
            $equipment->purchase_date = $request['purchase_date'];
            $equipment->guarantee = $request['guarantee'];
            $equipment->comment = ucfirst($request['comment']);
            if ($equipment->update()) {
                return redirect()->route('equipment', ['id' => $equipment->id])->with(['success' => 'You successfully changed  ' . $equipment->brandAndModel()]);
            }
            return redirect()->route('equipment', ['id' => $equipment->id])->with(['fail' => 'Something went wrong']);
        }
        return abort('404');
    }

    public function getChangeWorking($id)
    {
        if ($equipment = Equipment::find($id)) {
            return view('equipment.change_working_status', ['equipment' => $equipment]);
        }
        return abort('404');
    }


    public function patchChangeWorking(Request $request, $id)
    {
        if ($equipment = Equipment::find($id)) {
            $this->validate($request, [
                'working' => 'required'
            ]);
            $equipment->working = $request['working'];
            $equipment->write_off_date = null;
            if ($equipment->working == 2) {
                if ($request['write_off_date'] == null) {
                    return redirect()->route('change_working', ['id' => $id])->with(['fail' => 'If you want to write-off equipment you need to enter date']);
                }
                $equipment->write_off_date = $request['write_off_date'];
            }
            if ($equipment->update()) {
                return redirect()->route('equipment', ['id' => $id])->with(['success' => 'You successfully changed working status']);
            }
            return redirect()->route('equipment', ['id' => $id])->with(['fail' => 'Something went wrong']);
        }
        return abort('404');
    }

}
