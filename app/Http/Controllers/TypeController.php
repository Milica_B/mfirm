<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Type;
use App\Equipment;

class TypeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getTypes($working)
    {
        if ($working == '0' || $working == '1' || $working == '2') {
            $types = Type::paginate(10);

            return view('equipment.types', ['working' => $working, 'types' => $types]);
        }
        return abort('404');
    }

    public function getAddType($working)
    {
        if ($working == '0' || $working == '1' || $working == '2') {
            return view('equipment.add_type', ['working' => $working]);
        }
        return abort('404');
    }

    public function postAddType(Request $request, $working)
    {
        if ($working == '0' || $working == '1' || $working == '2') {
            $this->validate($request, [
                'name' => 'required'
            ]);
            $type = new Type();
            $type->name = ucfirst($request['name']);
            if ($type->save()) {
                return redirect()->route('types', ['working' => $working])->with(['success' => 'You successfully add new category']);
            }
            return redirect()->route('types', ['working' => $working])->with(['fail' => 'Something went wrong']);
        }
        return abort('404');
    }

    public function getEquipmentsByType($working, $type_id)
    {
        if (($working == '0' || $working == '1' || $working == '2') && $type = Type::find($type_id)) {
            $equipments = Equipment::where('type_id', $type_id)->where('working', $working)->orderBy('purchase_date','desc')->paginate(6);
            return view('equipment.equipments', ['equipments' => $equipments, 'type' => $type]);
        }
        return abort('404');
    }

    public function getDeleteType($working)
    {
        if ($working == '0' || $working == '1' || $working == '2') {
            $types = Type::all();
            return view('equipment.delete_type', ['working' => $working, 'types' => $types]);
        }
        return abort('404');
    }

    public function patchDeleteType(Request $request, $working)
    {
        if ($working == '0' || $working == '1' || $working == '2') {
            $this->validate($request, [
                'id' => 'required'
            ]);
            $type = Type::find($request['id']);
            if (count($type->equipments) !== 0) {
                return redirect()->route('types', ['working' => $working])->with(['fail' => 'You can not delete ' . $type->name . ', it contains  equipments']);
            }
            if ($type->delete()) {
                return redirect()->route('types', ['working' => $working])->with(['success' => 'You successfully deleted category ' . $type->name]);
            }
            return redirect()->route('types', ['working' => $working])->with(['fail' => 'Something went wrong']);
        }
        return abort('404');
    }

}
