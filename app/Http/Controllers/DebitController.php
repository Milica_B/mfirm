<?php

namespace App\Http\Controllers;

use App\Equipment;
use App\Employee;
use App\Debit;
use App\Type;
use Illuminate\Http\Request;

class DebitController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getDebitsForEmployee($id)
    {
        if ($employee = Employee::find($id)) {
            $debits = Debit::where('employee_id', $id)->orderBy('start_date', 'desc')->paginate(6);
            return view('debits.list_for_employee', ['debits' => $debits, 'employee' => $employee]);
        }
        return abort('404');
    }

    public function getDebitsForEquipment($id)
    {
        if ($equipment = Equipment::find($id)) {
            $debits = Debit::where('equipment_id', $id)->orderBy('start_date', 'desc')->paginate(6);
            return view('debits.list_for_equipment', ['debits' => $debits, 'equipment' => $equipment]);
        }
        return abort('404');
    }

    public function getAddDebitForEmployee($id)
    {
        if ($employee = Employee::find($id)) {
            $types = Type::with('equipments')->get();
            return view('debits.add_debit_for_employee', ['employee' => $employee, 'types' => $types]);
        }
        return abort('404');
    }

    public function getAddDebitForEquipment($id)
    {
        if ($equipment = Equipment::find($id)) {
            $employees = Employee::where('active', 1)->get();
            return view('debits.add_debit_for_equipment', ['equipment' => $equipment, 'employees' => $employees]);
        }
        return abort('404');
    }

    public function postAddDebitForEquipment(Request $request, $id)
    {
        $this->validate($request, [
            'employee_id' => 'required',
            'start_date' => 'required|date'
        ]);
        $debit = new Debit();
        $debit->start_date = $request['start_date'];
        $debit->comment = ucfirst(trim($request['comment']));
        $debit->employee_id = $request['employee_id'];
        $equipment = Equipment::find($id);
        if ($equipment->debits()->save($debit)) {
            return redirect()->route('debits_for_equipment', ['id' => $equipment->id])->with(['success' => 'You successfully added new debit for ' . $equipment->brandAndModel()]);
        }
        return redirect()->route('debits_for_equipment', ['id' => $equipment->id])->with(['fail' => 'Something went wrong']);
    }

    public function postAddDebitForEmployee(Request $request, $id)
    {
        if ($employee = Employee::find($id)) {
            $this->validate($request, [
                'equipment_id' => 'required',
                'start_date' => 'required|date'
            ]);
            $debit = new Debit();
            $debit->start_date = $request['start_date'];
            $debit->comment = ucfirst(trim($request['comment']));
            $debit->equipment_id = $request['equipment_id'];
            if ($employee->debits()->save($debit)) {
                return redirect()->route('debits_for_employee', ['id' => $employee->id])->with(['success' => 'You successfully added new debit for ' . $employee->fullName()]);
            }
            return redirect()->route('debits_for_employee', ['id' => $employee->id])->with(['fail' => 'Something went wrong']);
        }
        return abort('404');
    }

    public function getReturn($employee_or_equipment, $id)
    {
        if ($employee_or_equipment !== '0' && $employee_or_equipment !== '1') {
            return abort('404');
        }
        if ($debit = Debit::find($id)) {
            return view('debits.return', ['debit' => $debit, 'employee_or_equipment' => $employee_or_equipment]);
        }
        return abort(404);
    }

    public function patchReturn(Request $request, $employee_or_equipment, $id)
    {
        if ($employee_or_equipment !== '0' && $employee_or_equipment !== '1') {
            return abort('404');
        }
        if ($debit = Debit::find($id)) {
            $this->validate($request, [
                'end_date' => 'required|date'
            ]);
            $debit->end_date = $request['end_date'];
            $debit->comment .= PHP_EOL . ucfirst(trim($request['comment']));
            if ($debit->update()) {
                if ($employee_or_equipment == 0) {
                    return redirect()->route('debits_for_equipment', ['id' => $debit->equipment->id])->with(['success' => 'You successfully returned ' . $debit->equipment->brandAndModel()]);
                }
                if ($employee_or_equipment == 1) {
                    return redirect()->route('debits_for_employee', ['id' => $debit->employee->id])->with(['success' => $debit->employee->fullName() . ' successfully returned equipment']);
                }
            }
            if ($employee_or_equipment == 0) {
                return redirect()->route('debits_for_equipment', ['id' => $debit->equipment->id])->with(['fail' => 'Something went wrong']);
            }
            if ($employee_or_equipment == 1) {
                return redirect()->route('debits_for_employee', ['id' => $debit->employee->id])->with(['fail' => 'Something went wrong']);
            }
        }
        return abort('404');
    }

    public function getAddComment($employee_or_equipment, $id)
    {
        if ($employee_or_equipment !== '0' && $employee_or_equipment !== '1') {
            return abort('404');
        }
        if ($debit = Debit::find($id)) {
            return view('debits.add_to_comment', ['debit' => $debit, 'employee_or_equipment' => $employee_or_equipment]);
        }
        return abort('404');
    }

    public function patchAddComment(Request $request, $employee_or_equipment, $id)
    {
        if ($employee_or_equipment !== '0' && $employee_or_equipment !== '1') {
            return abort('404');
        }
        if ($debit = Debit::find($id)) {
            $debit->comment = $debit->comment . ' ' . ucfirst($request['comment']);
            if ($debit->update()) {
                if ($employee_or_equipment == 0) {
                    return redirect()->route('debits_for_equipment', ['id' => $debit->equipment->id])->with(['success' => 'You successfully added to comment for debit of ' . $debit->equipment->brandAndModel()]);
                }
                if ($employee_or_equipment == 1) {
                    return redirect()->route('debits_for_employee', ['id' => $debit->employee->id])->with(['success' => 'You successfully added to comment for debit of ' . $debit->employee->fullName()]);
                }
            }
            if ($employee_or_equipment == 0) {
                return redirect()->route('debits_for_equipment', ['id' => $debit->equipment->id])->with(['fail' => 'Something went wrong']);
            }
            if ($employee_or_equipment == 1) {
                return redirect()->route('debits_for_employee', ['id' => $debit->employee->id])->with(['fail' => 'Something went wrong']);
            }
        }
        return abort('404');
    }
}
