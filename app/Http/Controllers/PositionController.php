<?php

namespace App\Http\Controllers;

use App\Employee;
use Illuminate\Http\Request;
use App\Position;

class PositionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getPositions($id)
    {
        if ($employee = Employee::find($id)) {
            $positions = Position::where('employee_id', $id)->where('valid', '1')->orderBy('date','dsc')->paginate(8);
            return view('positions.index', ['employee' => $employee, 'positions' => $positions]);
        }
        return abort('404');
    }

    public function getNewPosition($id)
    {
        if ($employee = Employee::find($id)) {
            return view('positions.add', ['employee' => $employee]);
        }
        return abort('404');
    }

    public function postNewPosition(Request $request, $id)
    {
        if ($employee = Employee::find($id)) {
            $this->validate($request, [
                'name' => 'required|max:100',
                'type_of_employment' => 'required|max:100',
                'date' => 'required|date'
            ]);
            $position = new Position();
            $position->name = ucfirst($request['name']);
            $position->type_of_employment = ucfirst($request['type_of_employment']);
            $position->date = $request['date'];
            $position->comment = ucfirst($request['comment']);
            $position->valid = 1;
            If ($employee->positions()->save($position)) {
                return redirect()->route('positions', ['id' => $id])->with(['success' => 'You successfully added new position']);
            }
            return redirect()->route('positions', ['id' => $id])->with(['fail' => 'Something went wrong']);
        }
        return abort('404');
    }

    public function getPositionsWithCorrections($id)
    {
        if ($employee = Employee::find($id)) {
            $positions = Position::where('employee_id', $id)->orderBy('date','dsc')->paginate(5);
            return view('positions.with-correction', ['employee' => $employee, 'positions' => $positions]);
        }
        return abort('404');
    }

    public function getPositionChange($id, $position_id)
    {
        if ($employee = Employee::find($id)) {
            if ($position = position::find($position_id)) {
                return view('positions.comment', ['employee' => $employee, 'position' => $position]);
            }
        }
        return abort('404');
    }

    public function patchPositionChange(Request $request, $id, $position_id)
    {
        if ($employee = Employee::find($id)) {
            if ($position = position::find($position_id)) {
                $position->comment .= PHP_EOL .ucfirst($request['comment']);
                $position->valid = $request['valid'];
                if ($position->update()) {
                    return redirect()->route('positions_with_corrections', ['id' => $id])->with(['success' => 'You successfully changed position']);
                }
                return redirect()->route('positions_with_corrections', ['id' => $id])->with(['fail' => 'Something went wrong']);
            }
        }
        return abort('404');
    }
}
