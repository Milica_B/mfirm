<?php
namespace App\Http\controllers;

use App\Repair;
use App\Equipment;
use Illuminate\Http\Request;


class RepairController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getRepairs($id)
    {
        if ($equipment = Equipment::find($id)) {
            $repairs = $equipment->repairs()->orderBy('start_date','desc')->paginate(5);
            return view('repairs.index', ['equipment' => $equipment, 'repairs' => $repairs]);
        }
        return abort('404');
    }

    public function getAddRepair($id)
    {
        if ($equipment = Equipment::find($id)) {
            return view('repairs.add', ['equipment' => $equipment]);
        }
        return abort('404');
    }

    public function postAddRepair(Request $request, $id)
    {
        if ($equipment = Equipment::find($id)) {
            $this->validate($request, [
                'service' => 'required|max:100',
                'start_date' => 'date|required'
            ]);
            $repair = new Repair();
            $repair->service = ucfirst($request['service']);
            $repair->start_date = $request['start_date'];
            $repair->comment = ucfirst(trim($request['comment']));
            if ($equipment->repairs()->save($repair)) {
                return redirect()->route('repairs', ['id' => $equipment->id])->with(['success' => 'You successfully added new repair for ' . $equipment->brandAndModel()]);
            }
            return redirect()->route('repairs', ['id' => $equipment->id])->with(['fail' => 'Something went wrong']);
        }
        return abort('404');
    }

    public function getReturn($id)
    {
        if ($repair = Repair::find($id)) {
            return view('repairs.return', ['repair' => $repair]);
        }
        return abort('404');
    }

    public function patchReturn(Request $request, $id)
    {
        if ($repair = Repair::find($id)) {
            $this->validate($request, [
                'end_date' => 'required|date',
                'price' => 'required|min:0'
            ]);
            $repair->end_date = $request['end_date'];
            $repair->price = $request['price'];
            $repair->comment .= PHP_EOL . ucfirst(trim($request['comment']));
            if ($repair->update()) {
                return redirect()->route('repairs', ['id' => $repair->equipment->id])->with(['success' => 'You successfully returned ' . $repair->equipment->brandAndModel()]);
            }
            return redirect()->route('repairs', ['id' => $repair->equipment->id])->with(['fail' => 'Something went wrong']);
        }
        return abort('404');
    }

    public function getAddComment($id)
    {
        if ($repair = Repair::find($id)) {
            return view('repairs.add_to_comment', ['repair' => $repair]);
        }
        return abort('404');
    }

    public function patchAddComment(Request $request, $id)
    {
        if ($repair = Repair::find($id)) {
            if ($request['comment'] == null) {
                return redirect()->route('repairs', ['id' => $id]);
            }
            $repair->comment .= PHP_EOL . ucfirst(trim($request['comment']));
            If ($repair->update()) {
                return redirect()->route('repairs', ['id' => $repair->equipment->id])->with(['success' => 'You successfully added to comment']);
            }
            return redirect()->route('repairs', ['id' => $repair->equipment->id])->with(['fail' => 'Something went wrong']);
        }
        return abort('404');
    }
}
