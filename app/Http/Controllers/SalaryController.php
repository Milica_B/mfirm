<?php

namespace App\Http\Controllers;

use App\Employee;
use Illuminate\Http\Request;
use App\Salary;

class SalaryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getSalaries($id)
    {
        if ($employee = Employee::find($id)) {
            $match = ['employee_id' => $id, 'valid' => 1];
            $salaries = Salary::where($match)->orderBy('date','dsc')->paginate(8);
            return view('salaries.index', ['employee' => $employee, 'salaries' => $salaries]);
        }
        return abort('404');
    }

    public function getNewSalary($id)
    {
        if ($employee = Employee::find($id)) {
            return view('salaries.add', ['employee' => $employee]);
        }
        return abort('404');
    }

    public function postNewSalary(Request $request, $id)
    {
        if ($employee = Employee::find($id)) {
            $this->validate($request, [
                'amount' => 'numeric|min:0|required',
                'date' => 'required|date',
            ]);
            $salary = new Salary();
            $salary->amount = $request['amount'];
            $salary->date = $request['date'];
            $salary->valid = 1;
            $salary->comment = ucfirst(trim($request['comment']));
            if ($employee->salaries()->save($salary)) {
                return redirect()->route('salaries', ['id' => $id])->with(['success' => 'You successfully added new salary']);
            }
            return redirect()->route('salaries', ['id' => $id])->with(['fail' => 'Something went wrong']);
        }
        return abort('404');
    }

    public function getSalariesWithCorrections($id)
    {
        if ($employee = Employee::find($id)) {
            $salaries = Salary::where('employee_id', $id)->orderBy('date','dsc')->paginate(5);
            return view('salaries.with-correction', ['employee' => $employee, 'salaries' => $salaries]);
        }
        return abort('404');
    }

    public function getSalaryChange($id, $salary_id)
    {
        if ($employee = Employee::find($id)) {
            if ($salary = Salary::find($salary_id)) {
                return view('salaries.comment', ['employee' => $employee, 'salary' => $salary]);
            }
        }
        return abort('404');
    }

    public function patchSalaryChange(Request $request, $id, $salary_id)
    {
        if ($employee = Employee::find($id) && $salary = Salary::find($salary_id)) {
            $salary->comment .= PHP_EOL . ucfirst(trim($request['comment']));
            $salary->valid = $request['valid'];
            if ($salary->update()) {
                return redirect()->route('salary_with_corrections', ['id' => $id])->with(['success' => 'You successfully changed salary']);
            }
            return redirect()->route('salary_with_corrections', ['id' => $id])->with(['fail' => 'Something went wrong']);
        }
        return abort('404');
    }

}
