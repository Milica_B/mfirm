<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Debit extends Model
{
    protected $dates = ['start_date', 'end_date'];

    public function equipment()
    {
        return $this->belongsTo('App\Equipment');
    }

    public function employee()
    {
        return $this->belongsTo('App\Employee');
    }
}
