<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Equipment extends Model
{
    protected $dates = ['purchase_date', 'write_off_date'];

    public function type()
    {
        return $this->belongsTo('App\Type');
    }

    public function repairs()
    {
        return $this->hasMany('App\Repair');
    }

    public function debits()
    {
        return $this->hasMany('App\Debit');
    }

    public function guaranteeDate()
    {
        return $this->purchase_date->addMonths($this->guarantee);
    }

    public function brandAndModel()
    {
        return $this->brand . ' - ' . $this->model;
    }

    public function freeDebit()
    {
        $debits = $this->debits;
        foreach ($debits as $debit) {
            if ($debit->end_date == null) {
                return false;
            }
        }
        return true;
    }

    public function onRepair()
    {
        $repairs = $this->repairs;
        foreach ($repairs as $repair) {
            if ($repair->end_date == null) {
                return true;
            }
        }
        return false;
    }

}
