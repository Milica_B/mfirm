<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Salary extends Model
{
    protected $dates = ['date'];

    public function employee()
    {
        return $this->belongsTo('App\Employee');
    }
}
