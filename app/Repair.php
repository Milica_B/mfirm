<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Repair extends Model
{
    protected $dates = ['end_date', 'start_date'];

    public function equipment()
    {
        return $this->belongsTo('App\Equipment');
    }
}
