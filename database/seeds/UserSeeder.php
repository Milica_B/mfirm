<?php

use Illuminate\Database\Seeder;
use App\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user=new User();
        $user->first_name='Pegi';
        $user->last_name='Pegi';
        $user->email='pegi@pegi.com';
        $user->password=bcrypt('pegipegi');
        $user->save();
    }
}
