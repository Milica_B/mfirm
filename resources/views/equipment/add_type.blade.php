@extends('layouts.master')

@section('styles')
    <link rel="stylesheet" href="{{URL::asset('css/form.css')}}">
@stop

@section('menu')
    <p>Add new category</p>
@stop

@section('content')

    <form action="{{route('type_add',['working'=>$working])}}" method="post">
        {{csrf_field()}}

        <div class="input-group">
            <label for="name">Name</label>
            <input type="text" name="name" id="name" {{ $errors->has('date')?'class=has-error':''}}>
        </div>

        <button type="submit" class="btn"> Add new category</button>
    </form>
@stop
