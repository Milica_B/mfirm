@extends('layouts.master')

@section('styles')
    <link rel="stylesheet" href="{{URL::asset('css/form.css')}}">
@stop

@section('menu')
    <p> Change entries for
        <a href="{{route('equipment',['id'=>$equipment->id])}}"> {{$equipment->brandAndModel()}}</a>
    </p>
@stop

@section('content')
    @include('includes.info-box')

    <form action="{{route('equipment_edit',['id'=>$equipment->id])}}" method="post">
        {{method_field('PATCH')}}
        {{csrf_field()}}

        <div class="input-group">
            <label for="brand">Brand:</label>
            <input type="text" name="brand" id="brand"
                   {{ $errors->has('brand')?'class=has-error':''}} value="{{Request::old('brand')?Request::old('brand'):$equipment->brand}}"/>
        </div>

        <div class="input-group">
            <label for="model">Model:</label>
            <input type="text" name="model" id="model"
                   {{ $errors->has('model')?'class=has-error':''}} value="{{Request::old('model')?Request::old('model'):$equipment->model}}"/>
        </div>

        <div class="input-group">
            <label for="serial_number">Serial number:</label>
            <input type="text" name="serial_number" id="serial_number"
                   {{ $errors->has('serial_number')?'class=has-error':''}} value="{{Request::old('serial_number')?Request::old('serial_number'):$equipment->serial_number}}"/>
        </div>

        <div class="input-group">
            <label for="inventory_number">Inventory number:</label>
            <input type="text" name="inventory_number" id="inventory_number"
                   {{ $errors->has('inventory_number')?'class=has-error':''}} value="{{Request::old('inventory_number')?Request::old('inventory_number'):$equipment->inventory_number}}"/>
        </div>

        <div class="input-group">
            <label for="purchase_date">Purchase date:</label>
            <input type="date" name="purchase_date" id="purchase_date"
                   {{ $errors->has('purchase_date')?'class=has-error':''}} value="{{Request::old('purchase_date')?Request::old('purchase_date'):$equipment->purchase_date->toDateString()}}"/>
        </div>

        <div class="input-group">
            <label for="guarantee">Month's of guarantee:</label>
            <input type="number" name="guarantee" id="guarantee"
                   {{ $errors->has('guarantee')?'class=has-error':''}} value="{{Request::old('guarantee')?Request::old('guarantee'):$equipment->guarantee}}"/>
        </div>

        <div class="input-group">
            <label for="comment">Comment:</label>
            <textarea name="comment" rows="8" cols="15"
                      {{ $errors->has('comment')?'class=has-error':''}} id="comment">{{Request::old('comment')?Request::old('comment'):$equipment->comment}}</textarea>
        </div>

        <button type="submit" class="btn">Change</button>

    </form>

@stop
