@extends('layouts.master')

@section('menu')
    <p>{{$type->name}}</p>
@stop

@section('content')
    @include('includes.info-box')

    <div class="list">
        @foreach($equipments as $equipment)
            <article>
                <a href="{{route('equipment',['id'=>$equipment->id])}}">
                    <ul>
                        <li>{{$equipment->brand}}</li>
                        <li>{{$equipment->model}}</li>
                        <li>{{$equipment->serial_number}}</li>
                    </ul>
                </a>
            </article>
        @endforeach
    </div>

    <div class="links">
        <div class="link-box">
            <a href="{{route('add_equipment',['type_id'=>$type->id])}}"> Add new {{lcfirst($type->name)}} </a>
        </div>
    </div>
@stop

@section('pagination')
    {{ $equipments->links() }}
@stop