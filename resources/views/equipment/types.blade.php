@extends('layouts.master')

@section('content')
    @include('includes.info-box')

    <div class="list">
        @foreach($types as $type)
            <article>
                <a href="{{route('equipments',['working'=>$working,'type_id'=>$type->id])}}">
                    {{$type->name.' ('.$type->equipments->where('working',$working)->count() }})
                </a>
            </article>
        @endforeach
    </div>

    <div class="links">

        <div class="link-box">
            <a href="{{route('add_type',['working'=>$working])}}"> Add new category </a>
        </div>

        <div class="link-box">
            <a href="{{route('delete_type',['working'=>$working])}}"> Delete category </a>
        </div>
    </div>
@stop

@section('pagination')
    {{ $types->links()}}
@stop