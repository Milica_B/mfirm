@extends('layouts.master')

@section('styles')
    <link rel="stylesheet" href="{{URL::asset('css/form.css')}}">
@stop

@section('menu')
    <p>
        Change working status for
        <a href="{{route('equipment',['id'=>$equipment->id])}}"> {{$equipment->brandAndModel()}}</a>
    </p>

@stop

@section('content')
    @include('includes.info-box')

    <form action="{{route('working_change',['id'=>$equipment->id])}}" method="post">
        {{method_field('patch')}}
        {{csrf_field()}}

        <div class="input-group">
            <label for="working">Set working status: </label>
            <select name="working" id="working">
                <option value=0>Usable equipment</option>
                <option value=1>Defective equipment</option>
                <option value=2>Write-off equipment</option>
            </select>
        </div>

        <div class="input-group">
            <label for="write_off_date">If you want to write-off equipment, please enter date:</label>
            <input type="date" name="write_off_date" id="write_off_date"
                   {{ $errors->has('write_off_date')?'class=has-error':''}} value="{{isset($equipment->write_off_date)?$equipment->write_off_date->toDateString():''}}"/>
        </div>

        <button type="submit" class="btn">Change</button>
    </form>
@stop
