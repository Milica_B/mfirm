@extends('layouts.master')

@section('styles')
    <link rel="stylesheet" href="{{URL::asset('css/form.css')}}">
@stop

@section('menu')
    <p>Delete category</p>
@stop

@section('content')
    @include('includes.info-box')

    <form action="{{route('type_delete',['working'=>$working])}}" method="post">
        {{method_field('patch')}}
        {{csrf_field()}}

        <div class="input-group">
            <label for="id">Type:</label>
            <select name="id" id="id">
                @foreach($types as $type)
                    <option value="{{$type->id}}">{{$type->name}}</option>
                @endforeach
            </select>
        </div>

        <button type="submit" class="btn">Delete</button>
    </form>
@stop
