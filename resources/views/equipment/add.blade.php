@extends('layouts.master')

@section('styles')
    <link rel="stylesheet" href="{{URL::asset('css/form.css')}}">
@stop

@section('menu')
    <p>
        Add new <a href="{{route('equipments',['working'=>0,'type_id'=>$type->id])}}">{{lcfirst($type->name)}}</a>
    </p>
@stop

@section('content')
    @include('includes.info-box')

    <form action="{{route('equipment_add',['type_id'=>$type->id])}}" method="post">
        {{csrf_field()}}

        <div class="input-group">
            <label for="brand">Brand:</label>
            <input type="text" name="brand" id="brand"
                   {{$errors->has('brand')?'class=has-error':''}}  value="{{Request::old('brand')}}">
        </div>

        <div class="input-group">
            <label for="model">Model:</label>
            <input type="text" name="model" id="model"
                   {{$errors->has('model')?'class=has-error':''}}  value="{{Request::old('model')}}">
        </div>

        <div class="input-group">
            <label for="serial_number">Serial number:</label>
            <input type="text" name="serial_number" id="serial_number"
                   {{$errors->has('serial_number')?'class=has-error':''}}  value="{{Request::old('serial_number')}}">
        </div>

        <div class="input-group">
            <label for="inventory_number">Inventory number:</label>
            <input type="text" name="inventory_number" id="inventory_number"
                   {{$errors->has('inventory_number')?'class=has-error':''}}  value="{{Request::old('inventory_number')}}">
        </div>

        <div class="input-group">
            <label for="purchase_date">Purchase date:</label>
            <input type="date" name="purchase_date" id="purchase_date"
                   {{$errors->has('purchase_date')?'class=has-error':''}}  value="{{Request::old('purchase_date')}}">
        </div>

        <div class="input-group">
            <label for="guarantee">Month's of guarantee:</label>
            <input type="number" name="guarantee" id="guarantee"
                   {{$errors->has('guarantee')?'class=has-error':''}}  value="{{Request::old('guarantee')}}">
        </div>

        <div class="input-group">
            <label for="comment">Comment:</label>
            <textarea name="comment" rows="12" cols="50"
                      id="comment"{{$errors->has('guarantee')?'class=has-error':''}}>{{Request::old('comment')}}</textarea>
        </div>

        <button type="submit" class="btn">Add new {{lcfirst($type->name)}}</button>
    </form>
@stop
