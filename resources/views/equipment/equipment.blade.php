@extends('layouts.master')

@section('menu')
    <p>
        <a href="{{route('equipments',['working'=>$equipment->working,'type_id'=>$equipment->type_id])}}">
            {{$equipment->type->name}}</a> : {{$equipment->brandAndModel()}}
    </p>
@stop

@section('content')
    @include('includes.info-box')

    <div class="about-equipment">
        <p><b>Serial number: </b>{{$equipment->serial_number}}</p>
        <p><b>Inventory number: </b>{{$equipment->inventory_number}}</p>
        <p><b>Purchase date: </b>{{$equipment->purchase_date->format('j F Y')}}</p>
        <p><b>Guarantee until: </b>{{$equipment->guaranteeDate()->format('j F Y')}}</p>
        <p>
            @if($equipment->working==2)
                <b>Write-off date: </b>{{$equipment->write_off_date->format('j F Y')}}
            @endif
        </p>
        <p>
            <b> Comment: </b>{{$equipment->comment}}
        </p>
    </div>

    <div class="links-about-equipment">

        <div class="link-box">
            <a href="{{route('edit_equipment',['id'=>$equipment->id])}}">Change entries</a>
        </div>

        <div class="link-box">
            <a href="{{route('change_working',['id'=>$equipment->id])}}">Change working status</a>
        </div>

        <div class="link-box">
            <a href="{{route('repairs',['id'=>$equipment->id])}}">Repairs</a>
        </div>

        <div class="link-box">
            <a href="{{route('debits_for_equipment',['id'=>$equipment->id])}}">Debit</a>
        </div>
    </div>
@stop
