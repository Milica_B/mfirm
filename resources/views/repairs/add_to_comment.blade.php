@extends('layouts.master')

@section('styles')
    <link rel="stylesheet" href="{{URL::asset('css/form.css')}}">
@stop

@section('menu')
    <p>
        Add to comment for <a href="{{route('repairs',['id'=>$repair->equipment->id])}}">repair</a> of
        <a href="{{route('equipment',['id'=>$repair->equipment->id])}}">{{$repair->equipment->brandAndModel()}}</a>
    </p>
@stop

@section('content')
    @include('includes.info-box')

    <form action="{{route('comment_add',['id'=>$repair->id])}}" method="post">
        {{method_field('patch')}}
        {{csrf_field()}}

        <div class="input-group">
            <label for="comment"> Comment</label>
            <textarea name="comment" rows="8" cols="50"
                      id="comment" {{$errors->has('comment')?'class=has-error':''}}>{{Request::Old('comment')}}
            </textarea>
        </div>

        <button type="submit" class="btn">Add to comment</button>
    </form>
@stop
