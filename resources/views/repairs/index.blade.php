@extends('layouts.master')

@section('menu')
    <p>
        List of all repairs of
        <a href="{{route('equipment',['id'=>$equipment->id])}}">{{$equipment->brandAndModel()}}</a>
    </p>
@stop

@section('content')
    @include('includes.info-box')

    <table class="table5">
        <tr>
            <th> Start date</th>
            <th> Service</th>
            <th> End date</th>
            <th> Price</th>
            <th> Comment</th>
            <th> Return</th>
        </tr>

        @foreach($repairs as $repair)
            <tr>
                <td>{{$repair->start_date->format('j F Y')}}</td>
                <td class="td1">{{$repair->service}}</td>
                <td>{{$repair->end_date?$repair->end_date->format('j F Y'):''}}</td>
                <td>{{$repair->price}}</td>
                <td class="td2">{{$repair->comment}}</td>
                <td>
                    @if($repair->end_date==null)
                        <a href="{{route('get_return',['id'=>$repair->id])}}">Return</a>
                    @endif
                    @if($repair->end_date!==null)
                        <a href="{{route('add_comment',['id'=>$repair->id])}}">Add to comment</a>
                    @endif
                </td>
            </tr>
        @endforeach
    </table>

    @if(!$equipment->onRepair())
        <div class="links">
            <div class="link-box">
                <a href="{{route('add_repair',['id'=>$equipment->id])}}">Add new repair</a>
            </div>
        </div>
    @endif
@stop

@section('pagination')
    {{ $repairs->links() }}
@stop
