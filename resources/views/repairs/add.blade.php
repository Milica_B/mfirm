@extends('layouts.master')

@section('styles')
    <link rel="stylesheet" href="{{URL::asset('css/form.css')}}">
@stop

@section('menu')
    <p>
        Add new <a href="{{route('repairs',['id'=>$equipment->id])}}">repair</a> for
        <a href="{{route('equipment',['id'=>$equipment->id])}}">{{$equipment->brandAndModel()}}</a>
    </p>
@stop

@section('content')
    @include('includes.info-box')

    <form action="{{route('repair_add',['id'=>$equipment->id])}}" method="post">
        {{csrf_field()}}

        <div class="input-group">
            <label for="service">Service:</label>
            <input type="text" name="service" id="service"
                   {{$errors->has('service')?'class=has-error':''}} value="{{Request::old('service')}}">
        </div>

        <div class="input-group">
            <label for="start_date">Start date:</label>
            <input type="date" name="start_date" id="start_date"
                   {{$errors->has('start_date')?'class=has-error':''}} value="{{Request::old('start_date')}}">
        </div>

        <div class="input-group">
            <label for="comment">Comment</label>
            <textarea name="comment" rows="8" cols="40"
                      id="comment" {{$errors->has('comment')?'class=has-error':''}}>{{Request::old('comment')}}
            </textarea>
        </div>

        <button type="submit" class="btn">Add</button>
    </form>
@stop
