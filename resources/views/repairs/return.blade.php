@extends('layouts.master')

@section('styles')
    <link rel="stylesheet" href="{{URL::asset('css/form.css')}}">
@stop
@section('menu')
    <p>
        Returning
        <a href="{{route('equipment',['id'=>$repair->equipment->id])}}"> {{$repair->equipment->brandAndModel()}}</a>
        from <a href="{{route('repairs',['id'=>$repair->equipment->id])}}">repair</a>
    </p>
@stop

@section('content')
    @include('includes.info-box')

    <form action="{{route('patch_return',['id'=>$repair->id])}}" method="post">
        {{method_field('patch')}}
        {{csrf_field()}}

        <div class="input-group">
            <label for="end_date">End date</label>
            <input type="date" name="end_date" id="end_date"
                   {{$errors->has('end_date')?'class=has-error':''}} value="{{Request::old('end_date')}}">
        </div>

        <div class="input-group">
            <label for="price">Price</label>
            <input type="number" name="price" step="0.01" id="price"
                   {{$errors->has('price')?'class=has-error':''}} value="{{Request::old('price')}}">
        </div>

        <div class="input-group">
            <label for="comment">Comment</label>
            <textarea name="comment" rows="8" cols="30"
                      {{$errors->has('comment')?'class=has-error':''}} id="comment">{{Request::old('comment')}}</textarea>
        </div>

        <button type="submit" class="btn"> Return</button>
    </form>
@stop
