@extends('layouts.master')

@section('menu')
 <p>  {{$employee->fullName()}}</p>
@stop

@section('content')
    @include('includes.info-box')

    <div class="about-employee">
        <p><b>E-mail: </b>{{$employee->email}}</p>
        <p><b>Phone: </b>{{$employee->phone}}</p>
        <p><b>Municipality: </b>{{$employee->municipality}}</p>
        <p><b>Birthday: </b>{{$employee->birthday->format('j F Y')}}</p>
        <p><b>Start date: </b>{{$employee->start_date->format('j F Y')}}</p>
        <p>
            @if($employee->active==0)
                <b>End date: </b>{{$employee->end_date->format('j F Y')}}
            @endif
        </p>
    </div>

    <div class="links-about-employee">

        <div class="link-box">
            <a href="{{route('change_entries',['id'=>$employee->id])}}">Change entries</a>
        </div>

        <div class="link-box">
            <a href="{{route('salaries',['id'=>$employee->id])}}">Salaries</a>
        </div>

        <div class="link-box">
            <a href="{{route('positions',['id'=>$employee->id])}}">Positions</a>
        </div>

        <div class="link-box">
            <a href="{{route('debits_for_employee',['id'=>$employee->id])}}">Used equipment</a>
        </div>

        <div class="link-box">
            @if($employee->active==1)
                <a href="{{route('quit_job',['id'=>$employee->id])}}">Quit job</a>
            @endif
            @if($employee->active==0)
                <a href="{{route('work_again',['id'=>$employee->id])}}">Start again</a>
            @endif
        </div>
    </div>
@stop
