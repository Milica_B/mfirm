@extends('layouts.master')

@section('content')
    @include('includes.info-box')

    <div class="list">
        @foreach($employees as $employee)
            <article>
                <a href="{{route('employee_index',['id'=>$employee->id])}}"> {{$employee->fullName()}} </a>
            </article>
        @endforeach
    </div>

    <div class="list-with-link">
        @if($active==1)
            <a href="{{route('create_employee')}}"> <img src="{{URL::to('images/employeeplus.png')}}"/> </a>
        @endif
    </div>
@stop

@section('pagination')
    {{ $employees->links() }}
@stop