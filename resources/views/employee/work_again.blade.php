@extends('layouts.master')

@section('styles')
    <link rel="stylesheet" href="{{URL::asset('css/form.css')}}">
@stop

@section('menu')
    <p>Start again: <a href="{{route('employee_index',['id'=>$employee->id])}}"> {{$employee->fullName()}}</a></p>
@stop

@section('content')
    @include('includes.info-box')

    <form action="{{route('again_work',['id'=>$employee->id])}}" method="post">
        {{method_field('PATCH')}}
        {{csrf_field()}}

        <div class="input-group">
            <label for="start_date">Start date:</label>
            <input type="date" id="start_date" name="start_date"
                   {{ $errors->has('start_date')?'class=has-error':''}}  value="{{Request::old('start_date')}}"/>
        </div>

        <div class="input-group">
            <label for="amount">Salary:</label>
            <input type="number" step="0.01" id="amount" name="amount"
                   {{ $errors->has('amount')?'class=has-error':''}} value="{{Request::old('amount')}}"/>
        </div>

        <div class="input-group">
            <label for="comment_salary">Comment for salary:</label>
            <textarea name="comment_salary" id="comment_salary"
            rows="8" {{$errors->has('comment_salary')?'class=has-error':''}}
            cols="50">{{Request::old('comment_salary')}}</textarea>
        </div>

        <div class="input-group">
            <label for="name"> Position:</label>
            <input type="text" name="name" id="name"
                   {{ $errors->has('name')?'class=has-error':''}}  value="{{Request::old('name')}}"/>
        </div>

        <div class="input-group">
            <label for="type_of_employment"> Type of employment:</label>
            <input type="text" name="type_of_employment" id="type_of_employment"
                   {{ $errors->has('type_of_employment')?'class=has-error':''}}  value="{{Request::old('type_of_employment')}}"/>
        </div>

        <div class="input-group">
            <label for="comment_position">Comment for position:</label>
            <textarea name="comment_position" id="comment_position"
            rows="8" {{$errors->has('comment_position')?'class=has-error':''}}
            cols="50">{{Request::old('comment_position')}}</textarea>
        </div>

        <button type="submit" class="btn">Work</button>
    </form>
@stop
