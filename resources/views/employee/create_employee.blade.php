@extends('layouts.master')

@section('styles')
    <link rel="stylesheet" href="{{URL::asset('css/form.css')}}">
@stop

@section('menu')
    <p>Add new employee</p>
@stop

@section('content')
    @include('includes.info-box')

    <form action="{{route('employee_create')}}" method="post">
        {{csrf_field()}}

        <div class="input-group">
            <label for="name">Name:</label>
            <input type="text" name="name" id="name"
                   {{ $errors->has('name')?'class=has-error':''}} value="{{Request::old('name')}}"/>
        </div>

        <div class="input-group">
            <label for="last_name">Surname:</label>
            <input type="text" name="last_name" id="last_name"
                   {{ $errors->has('last_name')?'class=has-error':''}} value="{{Request::old('last_name')}}"/>
        </div>

        <div class="input-group">
            <label for="email">E-mail:</label>
            <input type="text" name="email" id="email"
                   {{ $errors->has('email')?'class=has-error':''}} value="{{Request::old('email')}}"/>
        </div>

        <div class="input-group">
            <label for="phone">Phone:</label>
            <input type="text" name="phone" id="phone"
                   {{ $errors->has('phone')?'class=has-error':''}} value="{{Request::old('phone')}}"/>
        </div>

        <div class="input-group">
            <label for="municipality">Municipality:</label>
            <input type="text" name="municipality" id="municipality"
                   {{ $errors->has('municipality')?'class=has-error':''}} value="{{Request::old('municipality')}}"/>
        </div>

        <div class="input-group">
            <label for="birthday">Birthday:</label>
            <input type="date" name="birthday" id="birthday"
                   {{ $errors->has('birthday')?'class=has-error':''}} value="{{Request::old('birthday')}}"/>
        </div>

        <div class="input-group">
            <label for="start_date">Start date:</label>
            <input type="date" name="start_date" id="start_date"
                   {{ $errors->has('start_date')?'class=has-error':''}} value="{{Request::old('start_date')}}"/>
        </div>

        <button type="submit" class="btn">Add new employee</button>
    </form>
@stop
