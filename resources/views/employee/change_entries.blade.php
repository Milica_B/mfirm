@extends('layouts.master')

@section('styles')
    <link rel="stylesheet" href="{{URL::asset('css/form.css')}}">
@stop

@section('menu')
    <p> Change entries for
        <a href="{{route('employee_index',['active'=>$employee->active,'id'=>$employee->id])}}"> {{$employee->fullName()}}</a>
    </p>
@stop

@section('content')
    @include('includes.info-box')

    <form action="{{route('entries_change',['active'=>$employee->active,'id'=>$employee->id])}}" method="post">
        {{method_field('PATCH')}}
        {{csrf_field()}}

        <div class="input-group">
            <label for="name">Name:</label>
            <input type="text" name="name" id="name" {{ $errors->has('name')?'class=has-error':''}} value="{{$employee->name}}"/>
        </div>

        <div class="input-group">
            <label for="last_name">Surname:</label>
            <input type="text" name="last_name" id="last_name"
                   {{ $errors->has('last_name')?'class=has-error':''}} value="{{$employee->last_name}}"/>
        </div>

        <div class="input-group">
            <label for="email">E-mail:</label>
            <input type="text" name="email" id="email"
                   {{ $errors->has('email')?'class=has-error':''}} value="{{$employee->email}}"/>
        </div>

        <div class="input-group">
            <label for="phone">Phone:</label>
            <input type="text" name="phone" id="phone"
                   {{ $errors->has('phone')?'class=has-error':''}} value="{{$employee->phone}}"/>
        </div>

        <div class="input-group">
            <label for="municipality">Municipality:</label>
            <input type="text" name="municipality" id="municipality"
                   {{ $errors->has('municipality')?'class=has-error':''}} value="{{$employee->municipality}}"/>
        </div>

        <div class="input-group">
            <label for="birthday">Birthday:</label>
            <input type="date" name="birthday" id="birthday"
                   {{ $errors->has('birthday')?'class=has-error':''}} value="{{$employee->birthday->toDateString()}}"/>
        </div>

        <div class="input-group">
            <label for="start_date">Start date:</label>
            <input type="date" name="start_date" id="start_date"
                   {{ $errors->has('start_date')?'class=has-error':''}} value="{{$employee->start_date->toDateString()}}"/>
        </div>

        @if($employee->active==0)
            <div class="input-group">
                <label for="end_date">End date:</label>
                <input type="date" name="end_date" id="end_date"
                       {{ $errors->has('end_date')?'class=has-error':''}} value="{{$employee->end_date->toDateString()}}"/>
            </div>
        @endif

        <button type="submit" class="btn">Change</button>
    </form>
@stop
