@extends('layouts.master')

@section('styles')
    <link rel="stylesheet" href="{{URL::asset('css/form.css')}}">
@stop

@section('menu')
    <p>
        Quit job:
        <a href="{{route('employee_index',['active'=>$employee->active,'id'=>$employee->id])}}">{{$employee->fullName()}}</a>
    </p>
@stop

@section('content')
    @include('includes.info-box')

    <form action="{{route('job_quit',['id'=>$employee->id])}}" method="post">
        {{method_field('PATCH')}}
        {{csrf_field()}}

        <div class="input-group">
            <label for="end_date">End date:</label>
            <input type="date" name="end_date" id="end_date"
                   {{ $errors->has('end_date')?'class=has-error':''}} value="{{Request::old('end_date')}}"/>
        </div>

        <div class="input-group">
            <label for="comment">Comment</label>
            <textarea name="comment" id="comment" rows="8" {{$errors->has('comment')?'class=has-error':''}}
            cols="50">{{Request::old('comment')}}</textarea>
        </div>
        <button type="submit" class="btn">Quit</button>
    </form>
@stop
