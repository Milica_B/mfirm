<div class="navigation">
    <p> All employees</p>
    <ul>
        <li {{Request::path()=='employees/1' || (isset($employee)?$employee->active==1:'')?'class=active':''}}>
            <a href="{{route('employees_list',['active'=>1])}}"> Current employees </a>
        </li>
        <li {{Request::path()=='employees/0' || (isset($employee)?$employee->active==0:'')?'class=active':''}}>
            <a href="{{route('employees_list',['active'=>0])}}"> Ex-employees </a>
        </li>
    </ul>
    <p> Equipment</p>
    <ul>
        <li {{Request::path()=='types/0' || (isset($equipment)?$equipment->working==0:'') || (isset($repair)?$repair->equipment->working==0:'')?'class=active':''}}>
            <a href="{{route('types',['working'=>0])}}"> Usable equipment </a>
        </li>
        <li {{Request::path()=='types/1' || (isset($equipment)?$equipment->working==1:'') || (isset($repair)?$repair->equipment->working==1:'')?'class=active':''?'class=active':''}}>
            <a href="{{route('types',['working'=>1])}}">Defective equipment </a>
        </li>
        <li {{Request::path()=='types/2' || (isset($equipment)?$equipment->working==2:'') || (isset($repair)?$repair->equipment->working==2:'')?'class=active':''?'class=active':''}}>
            <a href="{{route('types',['working'=>2])}}">Write-off equipment </a>
        </li>
    </ul>
</div>
