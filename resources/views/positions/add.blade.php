@extends('layouts.master')

@section('styles')
    <link rel="stylesheet" href="{{URL::asset('css/form.css')}}">
@stop

@section('menu')
    <p>
        Add new <a href="{{route('positions',['id'=>$employee->id])}}">position</a> for
        <a href="{{route('employee_index',['id'=>$employee->id])}}">{{$employee->fullName()}}</a>
    </p>
@stop

@section('content')
    @include('includes.info-box')

    <form action="{{route('position_add',['id'=>$employee->id])}}" method="post">
        {{csrf_field()}}

        <div class="input-group">
            <label for="name">Name:</label>
            <input type="text" name="name" id="name"
                   {{ $errors->has('name')?'class=has-error':''}} value="{{Request::old('name')}}"/>
        </div>

        <div class="input-group">
            <label for="type_of_employment">Type of employment:</label>
            <input type="text" name="type_of_employment" id="type_of_employment"
                   {{ $errors->has('type_of_employment')?'class=has-error':''}} value="{{Request::old('type_of_employment')}}"/>
        </div>

        <div class="input-group">
            <label for="date">Date:</label>
            <input type="date" name="date" id="date"
                   {{ $errors->has('date')?'class=has-error':''}} value="{{Request::old('date')}}"/>
        </div>

        <div class="input-group">
            <label for="comment">Comment:</label>
            <textarea name="comment" rows="12"
                      id="comment" {{ $errors->has('comment')?'class=has-error':''}} >{{Request::old('comment')}}
            </textarea>
        </div>

        <button type="submit" class="btn">Add new position</button>
    </form>
@stop
