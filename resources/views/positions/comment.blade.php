@extends('layouts.master')

@section('styles')
    <link rel="stylesheet" href="{{URL::asset('css/form.css')}}">
@stop

@section('menu')
    <p>
        Make change to <a href="{{route('positions',['id'=>$employee->id])}}">position</a>
        of <a href="{{route('employee_index',['id'=>$employee->id])}}">{{$employee->fullName()}}</a>
    </p>
@stop

@section('content')
    @include('includes.info-box')

    <form action="{{route('change_position',['id'=>$employee->id,'position_id'=>$position->id])}}" method="post">
        {{method_field('PATCH')}}
        {{csrf_field()}}

        <label for="valid">Valid:</label>
        <input type="radio" name="valid" value="1" checked id="valid">Yes
        <input class="radio" type="radio" name="valid" value="0" id="valid"/> No

        <div class="input-group">
            <label for="comment">Comment:</label>
            <textarea name="comment" rows="12"
                      id="comment" {{ $errors->has('comment')?'class=has-error':''}} >

            </textarea>
        </div>

        <button type="submit" class="btn">Add changes</button>

    </form>
@stop
