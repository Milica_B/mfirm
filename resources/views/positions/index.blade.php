@extends('layouts.master')

@section('menu')
    <p>
        Positions of <a href="{{route('employee_index',['id'=>$employee->id])}}"> {{$employee->fullName()}}</a>
    </p>
@stop

@section('content')
    @include('includes.info-box')

    <table class="table3">
        <tr>
            <th> Date</th>
            <th> Position</th>
            <th> Type of employment</th>
        </tr>
        @foreach($positions as $position)
            @if ($position->valid==1)
                <tr>
                    <td>{{$position->date->format('j F Y')}}</td>
                    <td>{{$position->name}}</td>
                    <td>{{$position->type_of_employment}}</td>
                </tr>
            @endif
        @endforeach
    </table>

    <div class="links">
        <div class="link-box">
            <a href="{{route('positions_with_corrections',['id'=>$employee->id])}}">Show all positions with errors
                and comments
            </a>
        </div>
        @if($employee->active==1)
            <div class="link-box">
                <a href="{{route('add_position',['id'=>$employee->id])}}">Add new position</a>
            </div>
        @endif
    </div>
@stop

@section('pagination')
    {{ $positions->links() }}
@stop