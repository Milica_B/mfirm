@extends('layouts.master')

@section('menu')
    <p>
        <a href="{{route('positions',['id'=>$employee->id])}}">Positions</a> with corrections of
        <a href="{{route('employee_index',['id'=>$employee->id])}}"> {{$employee->fullName()}}</a>
    </p>
@stop

@section('content')
    <table class="table4">
        <tr>
            <th> Date</th>
            <th> Name</th>
            <th> Type of employment</th>
            <th> Valid</th>
            <th> Comment</th>
            <th> Correct</th>
        </tr>
        @foreach($positions as $position)
            <tr>
                <td>{{$position->date->format('j F Y')}}</td>
                <td class="td1">{{$position->name}}</td>
                <td class="td1">{{$position->type_of_employment}}</td>
                <td>
                    @if($position->valid==1)
                        Correct
                    @endif
                    @if($position->valid==0)
                        Incorrect
                @endif
                <td class="td2"> {{$position->comment}}</td>
                <td>
                    <a href="{{route('position_change',['id'=>$employee->id,'position_id'=>$position->id])}}">Correct</a>
                </td>
            </tr>
        @endforeach
    </table>
@stop

@section('pagination')
    {{ $positions->links() }}
@stop