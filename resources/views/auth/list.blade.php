@extends('layouts.master')

@section('menu')
    <p>All users</p>
@stop

@section('content')
{{--    @include('includes.info-box')--}}

    <div class="list">
        @foreach($users as $user)
            <article>
                {{$user->first_name}} {{$user->last_name}}<br>
                Email: {{$user->email}}
            </article>
        @endforeach
    </div>

    <div class="list-with-link-user">
        <a href="{{url('/register')}}"> <img src="{{URL::to('images/userplus.png')}}"/> </a>
    </div>

@stop

@section('pagination')
    {{ $users->links() }}
@stop