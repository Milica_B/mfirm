@extends('layouts.app')

@section('content')
    <div class="center">
        <img src="{{URL::to('images/logomfirm.png')}}">

        @include('includes.info-box')
        @if (session('status'))
            <section class="info-box success">
                {{ session('status') }}
            </section>
        @endif

        <form class="form-horizontal" role="form" method="POST" action="{{ url('/password/reset') }}">
            {{ csrf_field() }}

            <input type="hidden" name="token" value="{{ $token }}">

            <div class="input-group">
                <label for="email">E-mail:</label>
                <input type="email" name="email" class="email" id="email"
                       {{ $errors->has('email')?'class=has-error':''}} value="{{Request::old('email')}}"/>
            </div>

            <div class="input-group">
                <label for="password">Password:</label>
                <input type="password" name="password" id="password"
                       {{ $errors->has('password')?'class=has-error':''}} value="{{Request::old('password')}}"/>
            </div>

            <div class="input-group">
                <label for="password_confirmation">Confirm password:</label>
                <input type="password" name="password_confirmation" id="password_confirmation"
                       {{ $errors->has('password_confirmation')?'class=has-error':''}} value="{{Request::old('password_confirmation')}}"/>
            </div>

            <button type="submit" class="btn">
                Reset Password
            </button>
        </form>
    </div>
@endsection
