@extends('layouts.app')

@section('content')

    <div class="center">
        <img  src="{{URL::to('images/logomfirm.png')}}" >

        @include('includes.info-box')
        @if (session('status'))
            <section class="info-box success">
                {{ session('status') }}
            </section>
        @endif

        <form class="form-horizontal" role="form" method="POST" action="{{ url('/password/email') }}">
            {{ csrf_field() }}

            <div class="input-group">
                <label for="email">E-mail:</label>
                <input type="email" name="email" class="email" id="email"
                       {{ $errors->has('email')?'class=has-error':''}} value="{{Request::old('email')}}"/>
            </div>

            <button type="submit" class="btn">
                Send Password Reset Link
            </button>
        </form>
    </div>
@endsection
