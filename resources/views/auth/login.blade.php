@extends('layouts.app')

@section('content')
    <div class="center">
        <img  src="{{URL::to('images/logomfirm.png')}}" >

        @include('includes.info-box')

        <form action="{{url('/login')}}" method="post">
            {{csrf_field()}}

            <div class="input-group">
                <label for="email">E-mail:</label>
                <input type="email" name="email" class="email" id="email"
                       {{ $errors->has('email')?'class=has-error':''}} value="{{Request::old('email')}}"/>
            </div>

            <div class="input-group">
                <label for="password">Password:</label>
                <input type="password" name="password" id="password"
                       {{ $errors->has('password')?'class=has-error':''}} value="{{Request::old('password')}}"/>
            </div>

            <div class="checkbox">
                <input type="checkbox" name="remember" id="remember"> <label for="remember">Remember Me</label>
            </div>

            <button type="submit" class="btn login">
                Login
            </button>

            <a href="{{ url('/password/reset') }}">
                Forgot Your Password?
            </a>
        </form>
    </div>
@endsection
