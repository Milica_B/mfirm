@extends('layouts.master')

@section('styles')
    <link rel="stylesheet" href="{{URL::asset('css/form.css')}}">
@stop

@section('menu')
    <p>Register new user </p>
@stop

@section('content')
    {{--@include('includes.info-box')--}}

    <form action="{{url('/register')}}" method="post">
        {{csrf_field()}}

        <div class="input-group">
            <label for="first_name">First name:</label>
            <input type="text" name="first_name" id="first_name"
                   {{ $errors->has('first_name')?'class=has-error':''}} value="{{Request::old('first_name')}}"/>
        </div>

        <div class="input-group">
            <label for="last_name">Last name:</label>
            <input type="text" name="last_name" id="last_name"
                   {{ $errors->has('last_name')?'class=has-error':''}} value="{{Request::old('last_name')}}"/>
        </div>

        <div class="input-group">
            <label for="email">E-mail address:</label>
            <input type="text" name="email" id="email"
                   {{ $errors->has('email')?'class=has-error':''}} value="{{Request::old('email')}}"/>
        </div>

        <div class="input-group">
            <label for="password">Password:</label>
            <input type="password" name="password" id="password"
                   {{ $errors->has('password')?'class=has-error':''}} value="{{Request::old('password')}}"/>
        </div>

        <div class="input-group">
            <label for="password_confirmation">Confirm password:</label>
            <input type="password" name="password_confirmation" id="password_confirmation"
                   {{ $errors->has('password_confirmation')?'class=has-error':''}} value="{{Request::old('password_confirmation')}}"/>
        </div>

        <button type="submit" class="btn">
            Register
        </button>
    </form>
@stop
