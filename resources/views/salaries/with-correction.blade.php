@extends('layouts.master')

@section('menu')
    <p>
        <a href="{{route('salaries',['id'=>$employee->id])}}">Salaries</a> with corrections of
        <a href="{{route('employee_index',['id'=>$employee->id])}}"> {{$employee->fullName()}}</a>
    </p>
@stop

@section('content')

    <table class="table2">
        <tr>
            <th> Date</th>
            <th> Value</th>
            <th> Valid</th>
            <th> Comment</th>
            <th> Correct</th>
        </tr>

        @foreach($salaries as $salary)
            <tr>
                <td>{{$salary->date->format('j F Y')}}</td>
                <td>{{$salary->amount}}</td>
                <td>
                    @if($salary->valid==1)
                        Correct
                    @endif
                    @if($salary->valid==0)
                        Incorrect
                    @endif
                </td>
                <td class="td2"> {{$salary->comment}}</td>
                <td><a href="{{route('salary_change',['id'=>$employee->id,'salary_id'=>$salary->id])}}"> Correct </a>
                </td>
            </tr>
        @endforeach
    </table>
@stop

@section('pagination')
    {{ $salaries->links() }}
@stop
