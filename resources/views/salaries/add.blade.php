@extends('layouts.master')

@section('styles')
    <link rel="stylesheet" href="{{URL::asset('css/form.css')}}">
@stop

@section('menu')
    <p>
        Add new <a href="{{route('salaries',['id'=>$employee->id])}}">salary</a>
        for <a href="{{route('employee_index',['id'=>$employee->id])}}">{{$employee->fullName()}}</a>
    </p>
@stop

@section('content')
    @include('includes.info-box')

    <form action="{{route('salary_add',['id'=>$employee->id])}}" method="post">
        {{csrf_field()}}

        <div class="input-group">
            <label for="date">Date:</label>
            <input type="date" name="date" id="date"
                   {{ $errors->has('date')?'class=has-error':''}} value="{{Request::old('date')}}"/>
        </div>

        <div class="input-group">
            <label for="amount">Amount:</label>
            <input type="number" name="amount" id="amount" step="0.01"
                   {{ $errors->has('amount')?'class=has-error':''}} value="{{Request::old('amount')}}"/>
        </div>

        <div class="input-group">
            <label for="comment">Comment:</label>
            <textarea name="comment" rows="12" id="comment" {{ $errors->has('comment')?'class=has-error':''}} >
                {{Request::old('comment')}}
            </textarea>
        </div>

        <button type="submit" class="btn">Add new salary</button>
    </form>
@stop
