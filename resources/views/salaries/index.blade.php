@extends('layouts.master')

@section('menu')
    <p>
        Salaries of
        <a href="{{route('employee_index',['id'=>$employee->id])}}"> {{$employee->fullName()}}</a>
    </p>
@stop

@section('content')
    @include('includes.info-box')

    <table class="table1">
        <tr>
            <th> Date</th>
            <th> Value</th>
        </tr>
        @foreach($salaries as $salary)
            @if ($salary->valid==1)
                <tr>
                    <td>{{$salary->date->format('j F Y')}}</td>
                    <td>{{$salary->amount}}</td>
                </tr>
            @endif
        @endforeach
    </table>

    <div class="links">
        <div class="link-box">
            <a href="{{route('salary_with_corrections',['id'=>$employee->id])}}">
                Show all salaries with errors and comments</a>
        </div>
        @if($employee->active==1)
            <div class="link-box">
                <a href="{{route('add_salary',['id'=>$employee->id])}}">Add new salary</a>
            </div>
        @endif
    </div>

@stop
@section('pagination')
    {{ $salaries->links() }}
@stop