@extends('layouts.master')

@section('styles')
    <link rel="stylesheet" href="{{URL::asset('css/form.css')}}">
@stop

@section('menu')
    <p>Add <a href="{{route('debits_for_equipment',['id'=>$equipment->id])}}">debit</a> for
        <a href="{{route('equipment',['id'=>$equipment->id])}}">{{$equipment->brandAndModel()}}</a>
    </p>
@stop

@section('content')
    @include('includes.info-box')

    <form action="{{route('debit_add_for_equipment',['id'=>$equipment->id])}}" method="post">
        {{csrf_field()}}

        <div class="input-group">
            <label for="employee_id">Select employee</label>
            <select name="employee_id" id="employee_id">
                @foreach($employees as $employee)
                    <option value="{{$employee->id}}">{{$employee->fullName()}}</option>
                @endforeach
            </select>
        </div>

        <div class="input-group">
            <label for="start_date">Start date</label>
            <input type="date" name="start_date" id="start_date"
                   {{$errors->has('start_date')?'class=has-error':''}} value="{{Request::old('start_date')}}">
        </div>

        <div class="input-group">
            <label for="comment">Comment</label>
            <textarea name="comment" rows="8" cols="50"
                      id="comment" {{$errors->has('comment')?'class=has-error':''}}>{{Request::old('comment')}}</textarea>
        </div>

        <button type="submit" class="btn">Add debit</button>
    </form>
@stop
