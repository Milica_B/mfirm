@extends('layouts.master')

@section('styles')
    <link rel="stylesheet" href="{{URL::asset('css/form.css')}}">
@stop

@section('menu')
    <p>Add to comment for
        @if($employee_or_equipment==0)
            <a href="{{route('debits_for_equipment',['id'=>$debit->equipment->id])}}">debit</a> of
            <a href="{{route('equipment',['id'=>$debit->equipment->id,'employee_or_equipment'=>0])}}">{{$debit->equipment->brandAndModel()}}</a>
        @endif
        @if($employee_or_equipment==1)
            <a href="{{route('debits_for_employee',['id'=>$debit->employee->id])}}">debit</a> of
            <a href="{{route('employee_index',['id'=>$debit->employee->id,'employee_or_equipment'=>1])}}">{{$debit->employee->fullName()}}</a>
        @endif
    </p>
@stop

@section('content')
    @include('includes.info-box')

    <form action="{{route('debit_comment_add',['id'=>$debit->id,'employee_or_equipment'=>$employee_or_equipment])}}"
          method="post">
        {{method_field('patch')}}
        {{csrf_field()}}

        <div class="input-group">
            <label for="comment"> Comment</label>
            <textarea name="comment" rows="8" cols="50"
                      id="comment" {{$errors->has('comment')?'class=has-error':''}}>{{Request::Old('comment')}}</textarea>
        </div>

        <button type="submit" class="btn">Add to comment</button>
    </form>
@stop
