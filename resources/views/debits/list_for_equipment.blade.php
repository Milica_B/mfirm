@extends('layouts.master')

@section('menu')
    <p> Debits of
        <a href="{{route('equipment',['id'=>$equipment->id])}}">{{$equipment->brandAndModel()}}</a>
    </p>
@stop

@section('content')
    @include('includes.info-box')

    <table class="table5">
        <tr>
            <th>Employee</th>
            <th>Start date</th>
            <th>End date</th>
            <th>Comment</th>
            <th></th>
        </tr>
        @foreach($debits as $debit)
            <tr>
                <td>
                    <a href="{{route('debits_for_employee',['id'=>$debit->employee->id])}}">
                        {{$debit->employee->fullName()}}</a>
                </td>

                <td>{{$debit->start_date->format('j F Y')}}</td>
                <td>{{$debit->end_date ? $debit->end_date->format('j F Y'):''}}</td>
                <td class="td2">{{$debit->comment}}</td>
                <td>

                    @if($debit->end_date==null)
                        <a href="{{route('return_debit',['id'=>$debit->id,'employee_or_equipment'=>0])}}">Return</a>
                    @endif
                    @if($debit->end_date!==null)
                        <a href="{{route('debit_comment_add',['id'=>$debit->id,'employee_or_equipment'=>0])}}">Add
                            to comment</a>
                    @endif

                </td>
            </tr>
        @endforeach
    </table>

    @if($equipment->freeDebit() && $equipment->working==0)
        <div class="links">
            <div class="link-box">
                <a href="{{route('add_debit_for_equipment',['id'=>$equipment->id])}}">
                    Add new debit
                </a>
            </div>
        </div>
    @endif

@stop
@section('pagination')
    {{ $debits->links() }}
@stop