@extends('layouts.master')

@section('styles')
    <link rel="stylesheet" href="{{URL::asset('css/form.css')}}">
@stop

@section('menu')
    <p>Add <a href="{{route('debits_for_employee',['id'=>$employee->id])}}">debit</a>  of
        <a href="{{route('employee_index',['id'=>$employee->id])}}">{{$employee->fullName()}}</a>
    </p>
@stop

@section('content')
    @include('includes.info-box')

    <form action="{{route('debit_add_for_employee',['id'=>$employee->id])}}" method="post">
        {{csrf_field()}}

        <div class="input-group">
            <label for="equipment_id">Select equipment</label>
            <select name="equipment_id" id="equipment_id">
                @foreach($types as $type)
                    <optgroup label="{{$type->name}}">
                        @foreach($type->equipments as $equipment)
                            @if($equipment->freeDebit() && $equipment->working==0)
                                <option value="{{$equipment->id}}">{{$equipment->brandAndModel()}}</option>
                            @endif
                        @endforeach
                    </optgroup>
                @endforeach
            </select>
        </div>

        <div class="input-group">
            <label for="start_date">Start date</label>
            <input type="date" name="start_date" id="start_date"
                   {{$errors->has('start_date')?'class=has-error':''}} value="{{Request::old('start_date')}}">
        </div>

        <div class="input-group">
            <label for="comment">Comment</label>
            <textarea name="comment" rows="8" cols="50"
                      id="comment" {{$errors->has('comment')?'class=has-error':''}}>{{Request::old('comment')}}</textarea>
        </div>

        <button type="submit" class="btn">Add debit</button>
    </form>
@stop
