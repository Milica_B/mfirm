@extends('layouts.master')

@section('menu')
    <p> Debits of
        <a href="{{route('employee_index',['id'=>$employee->id])}}">{{$employee->fullName()}}</a>
    </p>
@stop

@section('content')
    @include('includes.info-box')

    <table class="table5">
        <tr>
            <th>Equipment</th>
            <th>Start date</th>
            <th>End date</th>
            <th>Comment</th>
            <th></th>
        </tr>
        @foreach($debits as $debit)
            <tr>
                <td>
                    <a href="{{route('debits_for_equipment',['id'=>$debit->equipment->id])}}">
                        {{$debit->equipment->brandAndModel().'-'.$debit->equipment->serial_number}}</a>
                </td>
                <td>{{$debit->start_date->format('j F Y')}}</td>
                <td>{{$debit->end_date?$debit->end_date->format('j F Y'):''}}</td>
                <td class="td2">{{$debit->comment}}</td>
                <td>

                    @if($debit->end_date==null)
                        <a href="{{route('return_debit',['id'=>$debit->id,'employee_or_equipment'=>1])}}">Return</a>
                    @endif
                    @if($debit->end_date!==null)
                        <a href="{{route('debit_comment_add',['id'=>$debit->id,'employee_or_equipment'=>1])}}">Add
                            to comment</a>
                    @endif

                </td>
            </tr>
        @endforeach
    </table>

    @if($employee->active==1)
        <div class="links">
            <div class="link-box">
                <a href="{{route('add_debit_for_employee',['id'=>$employee->id])}}">
                    Add new debit
                </a>
            </div>
        </div>
    @endif
@stop

@section('pagination')
    {{ $debits->links() }}
@stop