@extends('layouts.master')

@section('styles')
    <link rel="stylesheet" href="{{URL::asset('css/form.css')}}">
@stop

@section('menu')
    <p>Returning from
        @if($employee_or_equipment==0) <a
                href="{{route('debits_for_equipment',['id'=>$debit->equipment->id,'empl'=>0])}}">debit</a>
        <a href="{{route('equipment',['id'=>$debit->equipment->id,'employee_or_equipment'=>1])}}">{{$debit->equipment->brandAndModel()}}</a>
        @endif
        @if($employee_or_equipment==1)
            <a href="{{route('debits_for_employee',['id'=>$debit->employee->id,'employee_or_equipment'=>1])}}">debit</a>
            of
            <a href="{{route('employee_index',['id'=>$debit->employee->id,'employee_or_equipment'=>1])}}">{{$debit->employee->fullName()}}</a>
        @endif
    </p>
@stop

@section('content')
    @include('includes.info-box')
    <form action="{{route('return_debit',['id'=>$debit->id,'employee_or_equipment'=>$employee_or_equipment])}}"
          method="post">
        {{method_field('patch')}}
        {{csrf_field()}}

        <div class="input-group">
            <label for="end_date">End date</label>
            <input type="date" name="end_date" id="end_date"
                   {{$errors->has('end_date')?'class=has-error':''}} value="{{Request::old('end_date')}}">
        </div>

        <div class="input-group">
            <label for="comment">Comment</label>
            <textarea name="comment" rows="8" cols="50"
                      {{$errors->has('comment')?'class=has-error':''}} id="comment">{{Request::old('comment')}}</textarea>
        </div>

        <button type="submit" class="btn">Return</button>
    </form>
@stop
