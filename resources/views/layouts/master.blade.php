<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/x-icon" href="{{URL::to('favicon.ico')}}">
    <title>MFirm</title>
    @yield('styles')
    <link rel="stylesheet" href="{{URL::asset('css/app.css')}}">
</head>

<body>
<div class="header">
    <div class="header-left">
        <a href="{{route('home')}}"><img src="{{URL::asset('images/logomfirm.png')}}"></a>
    </div>
    <div class="header-right">
        <div class="drop-down">
            <ul>
                <li>
                    @if(Auth::check())  {{Auth::user()->first_name}}
                    @endif
                    &#9660
                    <ul class="logout">
                        <li><a href="{{ url('/logout') }}"
                               onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">Logout</a>
                            <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </li>
                    </ul>
                </li>
            </ul>
            <a href="{{route('list_user')}}"><img src="{{URL::to('images/users.png')}}"/></a>
        </div>
    </div>
</div>

<div class="menu">
    <div class="center">
        @yield('menu')
    </div>
</div>

@include('includes.navigation')

<div class="main">
    @yield('content')
</div>
<div class="pagination">
    @yield('pagination')
</div>
</body>
</html>
