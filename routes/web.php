<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::get('/home', [
    'uses' => 'HomeController@getIndex',
    'as' => 'home'
]);
Route::get('/list', [
    'uses' => 'HomeController@getList',
    'as' => 'list_user'
]);
Route::get('/employee/create', [
    'uses' => 'EmployeeController@getCreateEmployee',
    'as' => 'create_employee'
]);
Route::post('/employee/create', [
    'uses' => 'EmployeeController@postCreateEmployee',
    'as' => 'employee_create'
]);
Route::get('/employees/{active}', [
    'uses' => 'EmployeeController@getEmployeesList',
    'as' => 'employees_list'
]);


Route::group(['prefix' => '/employee'], function () {
    Route::get('/{id}', [
        'uses' => 'EmployeeController@getEmployeeIndex',
        'as' => 'employee_index'
    ]);
    Route::get('/{id}/quit', [
        'uses' => 'EmployeeController@getQuitJob',
        'as' => 'quit_job'
    ]);
    Route::patch('/{id}/quit', [
        'uses' => 'EmployeeController@patchQuitJob',
        'as' => 'job_quit'
    ]);
    Route::patch('/{id}/change', [
        'uses' => 'EmployeeController@patchChangeEntries',
        'as' => 'entries_change'
    ]);
    Route::get('/{id}/edit', [
        'uses' => 'EmployeeController@getChangeEntries',
        'as' => 'change_entries'
    ]);
    Route::get('/{id}/work', [
        'uses' => 'EmployeeController@getWorkAgain',
        'as' => 'work_again'
    ]);
    Route::patch('/{id}', [
        'uses' => 'EmployeeController@patchWorkAgain',
        'as' => 'again_work'
    ]);
});

Route::group(['prefix' => '/salaries'], function () {
    Route::get('/{id}', [
        'uses' => 'SalaryController@getSalaries',
        'as' => 'salaries'
    ]);
    Route::get('/{id}/add', [
        'uses' => 'SalaryController@getNewSalary',
        'as' => 'add_salary'
    ]);
    Route::post('/{id}/add', [
        'uses' => 'SalaryController@postNewSalary',
        'as' => 'salary_add'
    ]);
    Route::get('/{id}/correction', [
        'uses' => 'SalaryController@getSalariesWithCorrections',
        'as' => 'salary_with_corrections'
    ]);
    Route::get('/{id}/change/{salary_id}', [
        'uses' => 'SalaryController@getSalaryChange',
        'as' => 'salary_change'
    ]);
    Route::patch('/{id}/change/{salary_id}/', [
        'uses' => 'SalaryController@patchSalaryChange',
        'as' => 'change_salary'
    ]);
});

Route::group(['prefix' => '/positions'], function () {
    Route::get('/{id}', [
        'uses' => 'PositionController@getPositions',
        'as' => 'positions'
    ]);
    Route::get('/{id}/add', [
        'uses' => 'PositionController@getNewPosition',
        'as' => 'add_position'
    ]);
    Route::post('/{id}/add', [
        'uses' => 'PositionController@postNewPosition',
        'as' => 'position_add'
    ]);
    Route::get('/{id}/correction', [
        'uses' => 'PositionController@getPositionsWithCorrections',
        'as' => 'positions_with_corrections'
    ]);
    Route::get('/{id}/change/{pid}', [
        'uses' => 'PositionController@getPositionChange',
        'as' => 'position_change'
    ]);
    Route::patch('/{id}/change/{pid}/', [
        'uses' => 'PositionController@patchPositionChange',
        'as' => 'change_position'
    ]);
});
Route::group(['prefix' => '/types'], function () {
    Route::get('/{working}', [
        'uses' => 'TypeController@getTypes',
        'as' => 'types'
    ]);
    Route::get('/{working}/add', [
        'uses' => 'TypeController@getAddType',
        'as' => 'add_type'
    ]);
    Route::post('/{working}/add', [
        'uses' => 'TypeController@postAddType',
        'as' => 'type_add'
    ]);
    Route::get('/{working}/{type_id}/equipments', [
        'uses' => 'TypeController@getEquipmentsByType',
        'as' => 'equipments'
    ]);
    Route::get('/{working}/delete', [
        'uses' => 'TypeController@getDeleteType',
        'as' => 'delete_type'
    ]);
    Route::patch('/{working}/delete', [
        'uses' => 'TypeController@patchDeleteType',
        'as' => 'type_delete'
    ]);
});

Route::group(['prefix' => '/equipment'], function () {
    Route::get('/{type_id}/add', [
        'uses' => 'EquipmentController@getAddEquipment',
        'as' => 'add_equipment'
    ]);
    Route::post('/{type_id}/add', [
        'uses' => 'EquipmentController@postAddEquipment',
        'as' => 'equipment_add'
    ]);
    Route::get('/{id}', [
        'uses' => 'EquipmentController@getEquipment',
        'as' => 'equipment'
    ]);
    Route::get('/{id}/edit', [
        'uses' => 'EquipmentController@getEditEquipment',
        'as' => 'edit_equipment'
    ]);
    Route::patch('/{id}/edit', [
        'uses' => 'EquipmentController@patchEditEquipment',
        'as' => 'equipment_edit'
    ]);
    Route::get('/{id}/change_working', [
        'uses' => 'EquipmentController@getChangeWorking',
        'as' => 'change_working'
    ]);
    Route::patch('/{id}/change_working', [
        'uses' => 'EquipmentController@patchChangeWorking',
        'as' => 'working_change'
    ]);
});

Route::group(['prefix' => '/repairs'], function () {
    Route::get('/{id}', [
        'uses' => 'RepairController@getRepairs',
        'as' => 'repairs'
    ]);
    Route::get('/{id}/add', [
        'uses' => 'RepairController@getAddRepair',
        'as' => 'add_repair'
    ]);
    Route::post('/{id}/add', [
        'uses' => 'RepairController@postAddRepair',
        'as' => 'repair_add'
    ]);
    Route::get('/return/{id}', [
        'uses' => 'RepairController@getReturn',
        'as' => 'get_return'
    ]);
    Route::patch('/return/{id}', [
        'uses' => 'RepairController@patchReturn',
        'as' => 'patch_return'
    ]);
    Route::get('/comment/{id}', [
        'uses' => 'RepairController@getAddComment',
        'as' => 'add_comment'
    ]);
    Route::patch('/comment/{id}', [
        'uses' => 'RepairController@patchAddComment',
        'as' => 'comment_add'
    ]);
});

Route::group(['prefix' => '/debits'], function () {
    Route::get('equipment/{id}', [
        'uses' => 'DebitController@getDebitsForEquipment',
        'as' => 'debits_for_equipment'
    ]);
    Route::get('employee/{id}', [
        'uses' => 'DebitController@getDebitsForEmployee',
        'as' => 'debits_for_employee'
    ]);
    Route::get('equipment/{id}/add', [
        'uses' => 'DebitController@getAddDebitForEquipment',
        'as' => 'add_debit_for_equipment'
    ]);
    Route::get('employee/{id}/add', [
        'uses' => 'DebitController@getAddDebitForEmployee',
        'as' => 'add_debit_for_employee'
    ]);
    Route::post('equipment/{id}/add', [
        'uses' => 'DebitController@postAddDebitForEquipment',
        'as' => 'debit_add_for_equipment'
    ]);
    Route::post('employee/{id}/add', [
        'uses' => 'DebitController@postAddDebitForEmployee',
        'as' => 'debit_add_for_employee'
    ]);
    Route::get('{employee_or_equipment}/return/{id}', [
        'uses' => 'DebitController@getReturn',
        'as' => 'return_debit'
    ]);
    Route::patch('{employee_or_equipment}/return/{id}', [
        'uses' => 'DebitController@patchReturn',
        'as' => 'debit_return'
    ]);
    Route::get('{employee_or_equipment}/comment/{id}', [
        'uses' => 'DebitController@getAddComment',
        'as' => 'debit_add_comment'
    ]);
    Route::patch('{employee_or_equipment}/comment/{id}', [
        'uses' => 'DebitController@patchAddComment',
        'as' => 'debit_comment_add'
    ]);
});


